﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Evaluacion02.Startup))]
namespace Evaluacion02
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
