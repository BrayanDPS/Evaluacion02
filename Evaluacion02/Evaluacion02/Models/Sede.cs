﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion02.Models
{
    public class Sede
    {
        [Key]
        public int idHeadquarters { get; set; }

        [Required(ErrorMessage = "Campo requerido ")]
        [Display(Name = "Name Headquarters")]
        public string nameHeadquarters { get; set; }

        public virtual ICollection<Instructor> Instructors { get; set;}
    }
}