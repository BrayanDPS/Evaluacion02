﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Evaluacion02.Models
{
    public class Evaluacion02Context : DbContext
    {
        public Evaluacion02Context() : base ("DefaultConnection")
        {

        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public System.Data.Entity.DbSet<Evaluacion02.Models.Instructor> Instructors { get; set; }

        public System.Data.Entity.DbSet<Evaluacion02.Models.Especialidad> Especialidads { get; set; }

        public System.Data.Entity.DbSet<Evaluacion02.Models.Sede> Sedes { get; set; }
    }
}