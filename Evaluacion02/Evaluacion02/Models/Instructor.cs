﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion02.Models
{
    public class Instructor
    {
        [Key]
        public int idInstructor { get; set; }

        [Required(ErrorMessage = "campo requerido ")]
        [Display(Name = "First Name")]
        public string firstName { get; set; }
        
        [Display(Name = "Second Name")]
        public string secondName { get; set; }
        
        [Display(Name = "First Lastname")]
        [Required(ErrorMessage = "campo requerido ")]
        public string firstLastname { get; set; }

        [Display(Name = "Second Lastname")]
        public string secondLastname { get; set; }

        [Required(ErrorMessage = "el campo es requerido")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@(outlook.com|gmail.com|zoho.com)$", ErrorMessage = "Correo Invalido")]
        [StringLength(30, ErrorMessage = "El campo {0} puede contener maximo {1} caracteres")]
        [Display(Name = "Email")]
        public string email { get; set; }

        [Required(ErrorMessage = "Campo requerido ")]
        [Display(Name = "Age")]
        public string age { get; set; }

        [Required(ErrorMessage = "Campo requerido ")]
        [Display(Name = "Birthdate")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime birthdate { get; set; }

        public int idHeadquarters { get; set; }
        public int idEspecialidad { get; set; }

        public virtual Especialidad especialidad { get; set; }
        public virtual Sede sede { get; set; }
        

    }
}