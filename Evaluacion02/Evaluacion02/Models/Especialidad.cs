﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion02.Models
{
    public class Especialidad
    {
        [Key]
        public int idEspecialidad { get; set; }

        [Required(ErrorMessage = "Campo requerido ")]
        [Display(Name = "Name")]
        public string name { get; set; }

        public virtual ICollection<Instructor> Instructors { get; set; }
    }
}